## Introduction

This guide walks you through the process of creating a simple Java webapp using Spring MVC for a healthcare provider (doctor, nurse, pharmacist, etc.) that is launched as a standalone app and queries the height observations for the patient in the EHR context. This simple webapp is a great starter project from which you can begin to add your services, business logic and user interface.

## Prerequisites

* An account on the HSPC Sandbox
* Understanding of the [HSPC Java Client](https://bitbucket.org/hspconsortium/java-client)
* (Optional) Familiarity with Spring MVC
* (Optional) Familiarity with the SMART on FHIR specification
* (Optional) Familiarity with the SMART Confidential Client
* (Optional) Familiarity with the HAPI query model

## What you will build

In this guide, you will build a simple Java Spring MVC webapp that consists of a JSP, Controller, and config files.  If you prefer, you can skip the steps in the guide and download a finished copy of the project.

## What you will need

* An IDE or text editor
* Java
* Maven 

## Running the Quick Start

### Step 1: Configure the App

See /src/main/resources/config/config.properties for a list of properties and their values.

You will need to update the clientId and clientSecret in your config.properties using what is given to you in Sandbox Manager.

Also, change the scopes in Sandbox Manager to these "launch launch/patient patient/*.read openid profile offline_access"

### Step 2: Build and run the App

````
> mvn package
> ...
> mvn jetty:run
> ...
````

### Step 3: Try it out

1. Go to the app's main page

    [http://localhost:8080](http://localhost:8080)
    
2. Enter your HSPC credentials (or the personal user credentials)

3. Select a patient such as Daniel A. Johnson

4. See the results from the HSPC Sandbox!
