/*
 * #%L
 * hspc-client-example
 * %%
 * Copyright (C) 2014 - 2015 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.hspconsortium.client.example.controller;

import ca.uhn.fhir.rest.gclient.IFetchConformanceUntyped;
import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.instance.model.api.IBaseConformance;
import org.hspconsortium.client.auth.access.IdToken;
import org.hspconsortium.client.auth.access.JsonAccessToken;
import org.hspconsortium.client.auth.authorizationcode.AuthorizationCodeRequestBuilder;
import org.hspconsortium.client.auth.credentials.ClientSecretCredentials;
import org.hspconsortium.client.controller.FhirEndpointsProviderSTU3;
import org.hspconsortium.client.example.AppConfig;
import org.hspconsortium.client.example.model.Height;
import org.hspconsortium.client.example.model.RefreshTokenResponse;
import org.hspconsortium.client.session.Session;
import org.hspconsortium.client.session.authorizationcode.AuthorizationCodeSessionFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
public class PatientHeightChartController {

    @Inject
    private AuthorizationCodeSessionFactory<ClientSecretCredentials> ehrSessionFactory;

    @Inject
    private AppConfig appConfig;

    @Inject
    private String fhirServicesUrl;

    @Inject
    private AuthorizationCodeRequestBuilder authorizationCodeRequestBuilder;


    @RequestMapping(value = "/patientHeightChart", method = RequestMethod.GET)
    public ModelAndView patientHeightChart(ModelAndView modelAndView, HttpSession httpSession, HttpServletRequest httpRequest) {
        // retrieve the EHR session from the http session
        Session ehrSession = (Session) httpSession.getAttribute(ehrSessionFactory.getSessionKey());

        // look up the user data
        JsonAccessToken accessToken = (JsonAccessToken)(ehrSession.getAccessToken());
        IdToken idToken = accessToken.getIdToken();
        modelAndView.addObject("idTokenClaims", idToken.getClaimsMap());

        // query the patient data
        Collection<Observation> heightObservations = ehrSession
                .getContextSTU3()
                .getPatientContext()
                .find(Observation.class, Observation.CODE.exactly().identifier("8302-2"));

        Patient patient = ehrSession.getContextSTU3().getPatientResource();

        modelAndView.setViewName("example");
        modelAndView.addObject("patientFullName", StringUtils.join(patient.getName().get(0).getGiven(), " ") + " " +
                patient.getName().get(0).getFamily());

        List<Height> heights = new ArrayList<>();
        for (Observation heightObservation : heightObservations) {
            String date = ((DateTimeType) heightObservation.getEffective()).getValueAsString();
            String height = ((Quantity) heightObservation.getValue()).getValue().toPlainString();
            heights.add(new Height(height, date));
        }

        modelAndView.addObject("heights", heights);
        return modelAndView;
    }

    @RequestMapping(value = "/refresh-access-token-example", method = RequestMethod.GET)
    public ModelAndView refreshAccessTokenExample(ModelAndView modelAndView, HttpSession httpSession, HttpServletRequest httpRequest) {
        // retrieve the EHR session from the http session
        Session ehrSession = (Session) httpSession.getAttribute(ehrSessionFactory.getSessionKey());

        FhirEndpointsProviderSTU3 fhirEndpointsProviderSTU3 = new FhirEndpointsProviderSTU3(ehrSession.getFhirContext());
        String refreshToken = ehrSession.getAccessToken().getRootResponse().get("refresh_token").getAsString();

        String newAccessToken = fetchNewAccessToken(refreshToken, fhirEndpointsProviderSTU3.getEndpoints(ehrSession.getServerBase()).getTokenEndpoint());

        modelAndView.addObject("newAccessToken", newAccessToken);
        modelAndView.setViewName("refreshAccessTokenExample");
        return modelAndView;
    }

    private String fetchNewAccessToken(String refreshToken, String tokenEndpoint) {
        String tokenUrl = tokenEndpoint;
        String clientId = appConfig.clientId();
        String clientSecret = appConfig.clientSecret();

        // set up basic authentication in a spring RestTemplate
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(clientId, clientSecret));

        // add body content with the appropriate header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("grant_type", "refresh_token");
        map.add("refresh_token", refreshToken);

        // create and execute request
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        ResponseEntity<RefreshTokenResponse> response = restTemplate.postForEntity(tokenUrl, request, RefreshTokenResponse.class);

        return response.getBody().getAccess_token();
    }

    @RequestMapping(value="/logout",method = RequestMethod.GET)
    public String logout(HttpServletRequest request){

        //the 2nd parameter is the url you would like to go to after signing out of the auth server. This is needed
        //because it is part of the request made to the auth server.
        return authorizationCodeRequestBuilder.endSession(fhirServicesUrl, "http://localhost:8080", request);
    }
}
